from os import listdir
from os.path import join
from ipdb import set_trace as st

IMG_EXTENSIONS = [
    '.jpg', '.JPG', '.jpeg', '.JPEG',
    '.png', '.PNG', '.ppm', '.PPM', '.bmp', '.BMP',
]


def is_image_file(filename):
    return any(filename.endswith(extension) for extension in IMG_EXTENSIONS)


def dataset_nyu(root, phase):
    rgb_path = join(root, 'rgb', phase)
    depth_path = join(root, 'depth', phase)
    paths_list = [(join(rgb_path, file_1), join(depth_path, file_2)) for (file_1, file_2) in zip(sorted(listdir(rgb_path)), sorted(listdir(depth_path))) if is_image_file(file_1) and is_image_file(file_2)]
    return paths_list  # for bayesian test: [350:500]

    # used to test outdoor images
    # rgb_path = join(root)
    # depth_path = join(root)
    # paths_list = [(join(rgb_path, file_1), join(depth_path, file_2)) for (file_1, file_2) in zip(sorted(listdir(rgb_path)), sorted(listdir(depth_path))) if is_image_file(file_1) and is_image_file(file_2)]
    # return paths_list  # for bayesian test: [350:500]


def dataset_kitti(root, phase, opt):
    import glob
    phase_path = join(root, phase)

    if opt.kitti_split.find('rob') is not -1:
        if phase != 'test':
            if opt.kitti_split.find('_r') is not -1:
                index = '[2]'
            else:
                index = '[2,3]'

            # search for depth images
            depth_search = "2011_*_*_drive_*_sync/proj_depth/groundtruth/image_0{}/*.png".format(index)
            depth_files = sorted(glob.glob(join(phase_path, depth_search)))
        else:
            print('There is no corresponding depth maps for tests.')
    elif opt.kitti_split.find('eigen') is not -1:
        image_files = []
        file_ = open("config/kitti/eigen_{}_files_rob.txt".format(phase), "r")

        for f in file_:
            phase_path = phase_path = join(root, phase)
            filenames = f.rsplit(' ', 1)
            if opt.kitti_split.find('_r') is not -1:
                f = filenames[0]
                filepaths = sorted(glob.glob(join(root, '*', f.strip('\n'))))
                if filepaths:
                    image_files.append(filepaths[0])
            else:
                for f in filenames:
                    filepaths = sorted(glob.glob(join(root, '*', f.strip('\n'))))
                    if filepaths:
                        image_files.append(filepaths[0])

        # if phase == 'test':
        #     depth_files = image_files  # correct it to not impose the need of depth images
        # else:
        depth_files = [f.replace('sync/', 'sync/proj_depth/groundtruth/') for f in image_files]
        
    return list(zip(image_files, depth_files))


def dataset_oneraroom(root, phase):
    import glob
    # phase_path = join(root, phase)

    filepath = "config/oneraroom/onera_{}_files.txt".format(phase)

    image_files = []
    depth_files = []

    with open(filepath, 'r') as f:
        for line in f:
            print('Folder: {} in phase: {}'.format(line, phase))
            image_search = join(root, line.strip('\n'), 'image', '*.jpg')
            depth_search = join(root, line.strip('\n'), 'depth', '*.png')

            [image_files.append(imagepath) for imagepath in sorted(glob.glob(image_search))]
            [depth_files.append(depthpath) for depthpath in sorted(glob.glob(depth_search))]

    return [(image_file, depth_file) for (image_file, depth_file) in zip(image_files, depth_files)]


def dataset_3drms_list(root, phase, index, ext=''):
    import glob
    phase_path = join(root, phase)
    scene_folders = '*{}'.format(index)
    vcam_ids = '*[02468]'
    file_ext = '*{}'.format(ext)
    return sorted(glob.glob(join(phase_path, scene_folders, vcam_ids, file_ext)))


def dataset_3drms_(root, phase): # phase = [test,train,val]_split
    import glob
    # This dataset does not have depth maps neither for validation, neither for tests
    if 'train' in phase:
        if '1' in phase:
            index = '[0][012][026][048]' # [0128,0160,0224]
        elif '2' in phase:
            index = '[0][01][026][018]' # [0001,0128,0160]
        else:  # train on all dataset
            index = ''
        return index
    elif 'val' in phase:
        if '1' in phase:
            index = '[0][0][0][1]'
        elif '2' in phase:
            index = '[0][2][2][4]'
        return index


def dataset_3drms(root, phase, use_semantics=False): # phase = [test,train,val]_split
    if 'test' in phase:
        phase='testing'
        phase_path = join(root, phase)
        image_files = sorted(glob.glob(join(phase_path, '*', '*', '*.png')))
        return list(zip(image_files, image_files))
    else:
        index = dataset_3drms_(root, phase)
        phase='training'
        image_list = dataset_3drms_list(root, phase, index, ext='undist.png')
        depth_list = dataset_3drms_list(root, phase, index, ext='.bin')
        if use_semantics:
            semantics_list = dataset_3drms_list(root, phase, index, ext='gtr.png')
            return list(zip(image_list, depth_list, semantics_list))
        return list(zip(image_list, depth_list))

def dataset_3drms_stereo(root, phase, use_semantics=False):
    if 'test' in phase:
        phase='testing'
        phase_path = join(root, phase)
        image_files = sorted(glob.glob(join(phase_path, '*', '*', '*.png')))
        return list(zip(image_files, image_files))
    else:
        index = dataset_3drms_(root, phase)
        phase='training'
        image_list = dataset_3drms_list(root, phase, index, ext='undist.png')
        depth_list = dataset_3drms_list(root, phase, index, ext='.bin')
        stereo_depth_list = dataset_3drms_list(root, phase='depth_from_stereo/sgbm_depth_map/training', index=index, ext='dmap.png')
        if use_semantics:
            semantics_list = dataset_3drms_list(root, phase, index, ext='gtr.png')
            return list(zip(image_list, depth_list, semantics_list, stereo_depth_list))
        return list(zip(image_list, depth_list, stereo_depth_list))


    
