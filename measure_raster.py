from options.raster_options import RasterOptions
from modules.errormeasurement_raster import ErrorMeasure
from ipdb import set_trace as st

opt = RasterOptions().parse()
errorob = ErrorMeasure(opt)
errorob.initialize()
errorob.calculate_all()
