from options.NYUOptions import Options
from modules.errormeasurement_cityscapes import ErrorMeasure
from ipdb import set_trace as st

opt = Options().parse()
from modules.eval_nyuv2 import ErrorMeasure

errorob = ErrorMeasure(opt)
errorob.initialize()

if opt.task == 'regression':
    errorob.calculate_regression()
elif opt.task == 'semantics':
    errorob.calculate_semantics()


