#!/bin/bash

clear
echo "This script gets the DATA ROOT, NAME of the experiment and EPOCHS to run test and write in file"


# DATA_ROOT=/data2/mcarvalh/softs/pix2pix/datasets/nyu_osplit/rgb_depth/ name=$1 which_direction=AtoB phase=test th /data2/mcarvalh/softs/pix2pix/test_minmax.lua

python /data2/mcarvalh/softs/scripts/error_measurement/error_measurement_mod.py --name $1 --data_root $2 --epoch $3

