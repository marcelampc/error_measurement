# Measure error, get images from folder, transform to color and save in chosen folder

# common opt file
# get options
from options.MSOptions import MSOptions
from modules.graytocmap import GrayCMap
from ipdb import set_trace as st

opt = MSOptions().parse()
from modules.errormeasurement_new import ErrorMeasure
# if 'rob' in opt.which_dataset:
#     from modules.errormeasurement import ErrorMeasure
# if opt.which_dataset == 'nyu' or opt.which_dataset == 'make3d' or opt.which_dataset == 'kitti':
#     from modules.errormeasurement_noconc import ErrorMeasure
# elif opt.which_dataset == 'ddff':
#     from modules.ddff_errormeasurement import ErrorMeasure

# Measure errorprint('oi')
errorob = ErrorMeasure(opt)
errorob.initialize()
errorob.calculate_all()
# errorob.calculate_per_iter()
# ranking_dict = errorob.get_ranking_list()
# st()
# # # gets names to color and save
# ranked_names_list = [name for name, value in ranking_dict]
# # st()
# gcm_obj = GrayCMap(opt)
# gcm_obj.initialize()
# gcm_obj.save_from_list(ranked_names_list)


# save a vector with the name of the best to the worst measurements
# which to pic? Precision?

# first: save a vector with the best and worst
