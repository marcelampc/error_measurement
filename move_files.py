from skimage.measure import structural_similarity as ssim
import matplotlib.pyplot as plt
import numpy as np
import os
from skimage import img_as_float, io
import argparse
import sys
import ipdb
from sklearn.metrics import mean_squared_error
from shutil import copyfile

# from_path = '/data2/mcarvalh/softs/pix2pix/results/nyu_osplit/100_net_G_test/images/'
from_path = '/data2/mcarvalh/softs/pix2pix/datasets/nyu_osplit/'
to_path = '/home/mcarvalh/DepthFromDefocus/Writing/Gretsi/2017/test_images/'

# subdirs = ['input', 'output', 'target', 'result']
subdirs = ['depth/test', 'rgb/test']
good_filenames = ['_0119.png', '_0252.png', '_0408.png', '_0414.png']
bad_filenames = ['_0640.png', '_0496.png']
# bad_filenames = ['rd_0640.png', 'rd_0496.png']

for s in subdirs:
	from_path_s = os.path.join(from_path, s)
	to_path_g_s = os.path.join(to_path+'good', s)
	os.makedirs(to_path_g_s, exist_ok=True)
	to_path_b_s = os.path.join(to_path+'bad', s)
	os.makedirs(to_path_b_s, exist_ok=True)
	if s == 'depth/test':
		ind = 'd'
	else:
		ind = 'r'
	for f in good_filenames:
		copyfile(os.path.join(from_path_s, ind+f), os.path.join(to_path_g_s, ind+f))
	for f in bad_filenames:
		copyfile(os.path.join(from_path_s, ind+f), os.path.join(to_path_b_s, ind+f))
