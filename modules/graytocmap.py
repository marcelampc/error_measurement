import matplotlib.pyplot as plt
import numpy as np
from skimage import img_as_float, io
import ipdb
import argparse
from PIL import Image
import os
from ipdb import set_trace as st
import shutil

# colormap: http://matplotlib.org/examples/color/colormaps_reference.html
# interpolation: http://matplotlib.org/examples/images_contours_and_fields/interpolation_methods.html

# python test.py --in_path='/data2/mcarvalh/softs/pix2pix/results/nyu_2103/latest_net_G_test/images/input/rd_0001.png' --gt_path='/data2/mcarvalh/softs/pix2pix/results/nyu_2103/latest_net_G_test/images/target/rd_0001.png' --pred_path='/data2/mcarvalh/softs/pix2pix/results/nyu_2103/latest_net_G_test/images/output/rd_0001.png' --name rd_0001 --cmap jet

class GrayCMap():
    def __init__(self, opt):
        self.opt = opt

    def initialize(self):
        if self.opt.data_root is not None:
            self.opt.dataroot = os.path.join(self.opt.data_root, 'results/' + self.opt.name  + '/'+ self.opt.epoch + '_net_G_' + self.opt.phase +'/images/')
            self.in_path = os.path.join(self.opt.dataroot, 'input/')
            self.gt_path = os.path.join(self.opt.dataroot, 'target/')
            self.pred_path = os.path.join(self.opt.dataroot, 'output/')

        if not os.path.exists(self.opt.results_path):
            os.makedirs(self.opt.results_path)
        
    def get_save_images_from_file(self, rank, filename):

        data = io.imread(os.path.join(self.gt_path, filename), as_grey=True)
        data2 = io.imread(os.path.join(self.pred_path, filename), as_grey=True)
        data_in = io.imread(os.path.join(self.in_path, filename))

        plt.figure()
        fig = plt.imshow(data, aspect='auto', interpolation=self.opt.interpolation, extent=[0,1,0,1], cmap=self.opt.cmap)
        plt.axis('off')
        fig.axes.get_xaxis().set_visible(False)
        fig.axes.get_yaxis().set_visible(False)
        plt.savefig('./results_'+self.opt.name+'/{0:04}_gt_{1}'.format(rank, filename), bbox_inches='tight', pad_inches=0)

        plt.figure()
        fig = plt.imshow(data2, aspect='auto', interpolation=self.opt.interpolation, extent=[0,1,0,1], cmap=self.opt.cmap)
        #plt.axis('tight')
        plt.axis('off')
        fig.axes.get_xaxis().set_visible(False)
        fig.axes.get_yaxis().set_visible(False)
        plt.savefig('./results_'+self.opt.name+'/{0:04}_out_{1}'.format(rank, filename), bbox_inches='tight', pad_inches=0)

    def get_save_images_from_folders():
        """
        """
        # Folders are in in_path, gt_path, pred_path


    def save_original_rgb(self, rank, filename):
        """
        """
        filename = filename.replace('rd_', 'r_')
        filename_dest = ('{0:04}_{1}'.format(rank, filename))
        ori_path = os.path.join(self.opt.data_root, 'datasets/nyu_osplit/rgb/test/')
        dest_path = ('./results_'+self.opt.name+'/')
        if not os.path.isdir(dest_path):
            os.makedirs(dest_path)
        shutil.copyfile(os.path.join(ori_path, filename), os.path.join(dest_path, filename_dest))
        ranked_rgb_only_path = os.path.join(dest_path, 'rgb')
        if not os.path.isdir(ranked_rgb_only_path):
            os.makedirs(ranked_rgb_only_path)

        # save to separate folder also to generate images for eigen and liu
        shutil.copyfile(os.path.join(ori_path, filename), os.path.join(ranked_rgb_only_path, filename_dest))


    def save_from_list(self, list):
        rank = 1
        for filename in list:
            # Save original rgb without changing data
            self.save_original_rgb(rank, filename)
            
            # Save prediction and ground truth
            self.get_save_images_from_file(rank, filename)
            rank += 1
