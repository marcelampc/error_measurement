# No concatenation
from PIL import Image
import numpy as np
import glob
from ipdb import set_trace as st
from tqdm import tqdm
from os.path import join

from sklearn.metrics import mean_squared_error as mse
from sklearn.metrics import mean_absolute_error as abse

size = 0


def load_data(path, gt_image=False, dtype=np.float32, interpolation=Image.BILINEAR):
    global size
    if gt_image == True:
        img_pil = Image.open(path)
        # img_np = np.array(crop_kitti_supervised(Image.open(path)))
        if(dtype==np.uint8): # have to crop for segmentation
            img_pil = img_pil.crop((41, 45, 602, 472))
        img_np = np.array(img_pil, dtype=dtype)
        size = (img_np.shape[1], img_np.shape[0])
    else: 
        img_np = np.array(Image.open(path).resize(size, interpolation), dtype=dtype)
    return img_np


def mask_invalid_pixels_depth(gt, pred):
    gt = np.ma.masked_equal(gt, 0.0)
    pred = np.ma.masked_array(pred, gt.mask)

    gt = np.ma.compressed(gt)
    pred = np.ma.compressed(pred)

    return gt, pred

class ErrorMeasure():
    def __init__(self, opt):
        self.opt = opt

    def initialize(self):
        from modules.dataset.dataset_bank import dataset_nyu
        output_files_list = sorted(glob.glob(join(self.opt.path_pred, self.opt.task, '*.png')))
        self.lists =  list(zip(dataset_nyu(self.opt.path_gt, self.opt.data_split, task=self.opt.task), output_files_list))

    def calculate_regression(self):
        abs_rel = 0
        sq_rel = 0
        rmse = 0
        rmse_log = 0
        abs_log = 0
        acc1 = 0
        acc2 = 0
        acc3 = 0
        for gt_path, out_path in tqdm(self.lists):
            gt_np = load_data(gt_path, gt_image=True) / self.opt.scale 
            out_np = load_data(out_path) / self.opt.scale 
            # out_np = load_data(out_path, gt_image=True) / self.opt.scale # / self.opt.scale_to_mm
            # gt_np = load_data(gt_path) / self.opt.scale # / self.opt.scale_to_mm

            gt_np, out_np = mask_invalid_pixels_depth(gt_np, out_np)

            # Classical evaluation measurements
            abs_rel += abse(gt_np / gt_np, out_np / gt_np)
            # sq_rel += np.mean(((gt_np - out_np) ** 2) / gt_np)
            rmse += np.sqrt(mse(gt_np, out_np))
            # rmse_log += np.sqrt(mse(np.log(gt_np), np.log(out_np)))
            # abs_log += abse(np.log10(gt_np), np.log10(out_np))
            thres = np.maximum((gt_np / out_np), (out_np / gt_np))
            acc1 += (thres < 1.25).mean()
            acc2 += (thres < 1.25 ** 2).mean()
            acc3 += (thres < 1.25 ** 3).mean()

            # KITTI error Measurements

        n_images = len(self.lists)
        abs_rel  /= n_images
        # sq_rel   /= n_images
        rmse     /= n_images
        # rmse_log /= n_images
        # abs_log   /= n_images
        acc1     /= n_images
        acc2     /= n_images
        acc3     /= n_images
        # print('Cap: {}'.format(self.opt.max_depth_mask))
        print('Name: {}'.format(self.opt.name))
        print('ABS_REL: {:.3f}'.format(abs_rel))
        # print('SQ_REL: {:.3f}'.format(sq_rel))
        print('RMSE: {:.3f}'.format(rmse))
        # print('RMSE_log: {:.3f}'.format(rmse_log))
        # print('ABS_log: {:.3f}'.format(abs_log))
        print('ACC1: {:.1f}\%'.format(acc1 * 100))
        print('ACC2: {:.1f}\%'.format(acc2 * 100))
        print('ACC3: {:.1f}\%'.format(acc3 * 100))
        print('{}: abs: {:.3f}  rms: {:.3f}  acc: {:.1f}\\% & {:.1f}\\% & {:.1f}\\%'.format(self.opt.name, abs_rel, rmse, acc1 * 100, acc2 * 100, acc3 * 100))
        # print('& {:.3f} & {:.3f} & {:.3f} && {:.1f}\\% & {:.1f}\\% & {:.1f}\\%'.format(abs_rel, rmse, rmse_log, acc1 * 100, acc2 * 100, acc3 * 100))

    # def load_segmap(self, filename):


    def calculate_semantics(self):
        n_classes = self.opt.n_classes
        from sklearn.metrics import confusion_matrix
        from sklearn.metrics import f1_score
        import semseg.metrics.raster as metrics
        global_cm = np.zeros((self.opt.n_classes-1, self.opt.n_classes-1))
        for gt_filename, pred_filename in tqdm(self.lists):
            # must encode segmap befor comparing
            gt_np = load_data(gt_filename, dtype=np.uint8, gt_image=True)
            out_np = load_data(pred_filename, dtype=np.uint8, interpolation=Image.BILINEAR)
            # out_np = load_data(pred_filename, dtype=np.uint8, gt_image=True, interpolation=Image.BILINEAR)
            # gt_np = load_data(gt_filename, dtype=np.uint8)
            cm = confusion_matrix(gt_np.ravel(), out_np.ravel(), labels=list(range(1, n_classes)))
            global_cm += cm
            # st()

        # scores
        overall_acc = metrics.stats_overall_accuracy(global_cm)
        average_acc, _ = metrics.stats_accuracy_per_class(global_cm)
        average_iou, _ = metrics.stats_iou_per_class(global_cm)
        f1_score_avg, f1_score_per_class = metrics.stats_f1score_per_class(global_cm)
        # print('Cap: {}'.format(self.opt.max_depth_mask))
        print('Name: {}'.format(self.opt.name))
        # print('Epoch: {}', format(self.opt.name))
        print('AF1: {:.1f}\\%'.format(f1_score_avg*100))
        print('OA: {:.1f}\\%'.format(overall_acc*100))
        print('AA: {:.1f}\\%'.format(average_acc*100))
        print('AIoU: {:.1f}\\%'.format(average_iou*100))

        print('& {:.1f}\\% & {:.1f}\\% & {:.1f}\\%'.format(overall_acc*100, average_acc*100, average_iou*100))

        print('\nF1 scores per class')
        string_f1_scores = ''
        for f1_score in f1_score_per_class:
            string_f1_scores += '& {:.1f}\\% '.format(f1_score*100)
        print(string_f1_scores)

        print('{} OA: {:.1f}\\%  AA: {:.1f}\\%  AIoU: {:.1f}\\%'.format(self.opt.name, overall_acc*100, average_acc*100, average_iou*100))
