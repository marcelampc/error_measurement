# No concatenation
from PIL import Image
import numpy as np
import glob
from ipdb import set_trace as st
from tqdm import tqdm
from os.path import join

from sklearn.metrics import confusion_matrix
from sklearn.metrics import f1_score
import semseg.metrics.raster as metrics
size = 0

def load_and_mask_depth(path, mask):
    import rasterio
    raster = rasterio.open(path).read()
    return np.ma.compressed(np.ma.masked_array(raster, mask.mask))

def mask_invalid_depth(depth):
    # depth = depth # bigger than the minimum 
    # mask numbers > depth_max
    depth_mask = (depth < 9000) # everest has 8.848m
    return depth * depth_mask # values to ignore will be 0 # thats ugly

def mask_invalid_depth_bigger_then(*array):
    # depth = depth # bigger than the minimum 
    # mask numbers > depth_max
    data_r = []
    for data in array:
        data_mask = (data > 0) # everest has 8.848m
        data_r.append(data * data_mask) # values to ignore will be 0 # thats ugly
    return data_r

class ErrorMeasure():
    def __init__(self, opt):
        self.opt = opt

    def initialize(self):
        # get filenames
        if 'ISPRS' in self.opt.path_gt:
            target_file_path, output_files_list = self.get_filenames(self.opt.phase, self.opt.path_gt, self.opt.path_pred, self.opt.which_raster, self.opt.use_semantics)
            from .dataset.dataset_raster import load_raster
            target_loader = [target for target in load_raster(target_file_path)] # shape (wxh)
            output_loader = [target[0] for target in load_raster(output_files_list)] # shape (1xwxh)
            self.lists = list(zip(target_loader, output_loader))

    def get_filenames(self, phase, path_gt, out_path, which_raster, use_semantics):
        if 'ISPRS' in path_gt:
            from .dataset.dataset_bank import dataset_vaihingen
            target_file_path = dataset_vaihingen(path_gt, data_split=phase, phase='test', model='semantics')
            output_files_list = sorted(glob.glob(join(out_path, 'semantics/out*.tif')))
            output_files_list = output_files_list[0:len(target_file_path)]
            return target_file_path, output_files_list

    def calculate_all(self):
        global_cm = 0
        n_classes = self.opt.n_classes - 1 # do not consider clutter
        if 'ISPRS' in self.opt.path_gt:
            for gt_np, out_np in tqdm(self.lists):
                cm = confusion_matrix(gt_np[0].ravel(), out_np.ravel(), labels=list(range(1, n_classes)))
                global_cm += cm

        # scores
        overall_acc = metrics.stats_overall_accuracy(global_cm)
        average_acc, _ = metrics.stats_accuracy_per_class(global_cm)
        average_iou, _ = metrics.stats_iou_per_class(global_cm)
        f1_score_avg, f1_score_per_class = metrics.stats_f1score_per_class(global_cm)
        # print('Cap: {}'.format(self.opt.max_depth_mask))
        print('Name: {}', format(self.opt.name))
        print('AF1: {:.1f}\\%'.format(f1_score_avg*100))
        print('OA: {:.1f}\\%'.format(overall_acc*100))
        print('AA: {:.1f}\\%'.format(average_acc*100))
        print('AIoU: {:.1f}\\%'.format(average_iou*100))

        print('& {:.1f}\\% & {:.1f}\\% & {:.1f}\\%'.format(overall_acc*100, average_acc*100, average_iou*100))

        print('\nF1 scores per class')
        string_f1_scores = ''
        for f1_score in f1_score_per_class:
            string_f1_scores += '& {:.1f}\\% '.format(f1_score*100)
        print(string_f1_scores)

        # Kitti evaluation

