# No concatenation
from PIL import Image
import numpy as np
import glob
from ipdb import set_trace as st
from tqdm import tqdm
from os.path import join

from sklearn.metrics import mean_squared_error as mse
from sklearn.metrics import mean_absolute_error as abse
# # from skimage.measure import compare_ssim as ssim

import seaborn as sns
import matplotlib.pyplot as plt
size = 0


def load_and_mask_depth(path, mask):
    import rasterio
    raster = rasterio.open(path).read()
    return np.ma.compressed(np.ma.masked_array(raster, mask.mask))

def mask_invalid_depth(depth):
    # depth = depth # bigger than the minimum 
    # mask numbers > depth_max
    depth_mask = (depth < 9000) # everest has 8.848m
    return depth * depth_mask # values to ignore will be 0 # thats ugly

def mask_invalid_depth_bigger_then(*array):
    # depth = depth # bigger than the minimum 
    # mask numbers > depth_max
    data_r = []
    for data in array:
        data_mask = (data > 0) # everest has 8.848m
        data_r.append(data * data_mask) # values to ignore will be 0 # thats ugly
    return data_r

class ErrorMeasure():
    def __init__(self, opt):
        self.opt = opt

    def initialize(self):
        if 'ISPRS' in self.opt.path_gt:
            target_file_path, output_files_list = self.get_filenames(self.opt.phase, self.opt.path_gt, self.opt.path_pred, self.opt.which_raster, self.opt.use_semantics)
            from .dataset.dataset_raster import load_raster
            target_loader = [target for target in load_raster(target_file_path)] 
            output_loader = [target for target in load_raster(output_files_list)] 
            self.lists = list(zip(target_loader, output_loader))
        else:
            rgb_list, target_file_path, output_files_list = self.get_filenames(self.opt.phase, self.opt.path_gt, self.opt.path_pred, self.opt.which_raster, self.opt.use_semantics)
            # load images
            from .dataset.dataset_raster import load_depths_and_masks
            # get mask
            target_loader = [(target, masks) for target, masks in load_depths_and_masks(rgb_list, target_file_path)] 

            if self.opt.normalize:
                self.min_v, self.max_v = self.get_min_max(target_file_path, self.opt.which_raster)
            self.lists = list(zip(target_loader, output_files_list))

    def get_filenames(self, phase, path_gt, out_path, which_raster, use_semantics):
        if 'ISPRS' in path_gt:
            from .dataset.dataset_bank import dataset_vaihingen
            target_file_path = dataset_vaihingen(path_gt, data_split=phase, phase='test', model='regression')
            output_files_list = sorted(glob.glob(join(out_path, 'height/out*.tif')))
            output_files_list = output_files_list[0:len(target_file_path)]
            return target_file_path, output_files_list
        else:
            from .dataset.dataset_bank import dataset_dfc
            # Load ground truth
            rgb_list_paths, target_file_path = dataset_dfc(path_gt, phase, which_raster=which_raster, use_semantics=use_semantics) 

            # Load output
            output_files_list = sorted(glob.glob(join(out_path, 'out*.tif')))
            # if len(output_files_list) > 10:
            output_files_list = output_files_list[0:len(rgb_list_paths)]
            return rgb_list_paths, target_file_path, output_files_list

    def normalize_raster(self, *array, min_v, max_v):
        # normalize between 0-1
        normalized_array = []
        for data in array:
            normalized_array.append((data - min_v) / (max_v - min_v))
        return normalized_array

    def get_min_max(self, target_path, which_raster):
        import rasterio
        # st()
        raster = [rasterio.open(path) for path in target_path]
        if which_raster != 'dsm':
            dsm, dem = raster[0].read(), raster[1].read()
            mask_dsm = (dsm < 9000) * (dem < 9000)
            height = mask_dsm * (dsm - dem)
            return height.max(), height.min()
        else:
            raster_np = mask_invalid_depth(raster[0].read())
            return raster_np.max(), raster_np.min()

    def plot(self, gt, out, delta=0.01):
        sns.set()
        errors_by_range = []
        index=[]
        plt.figure()

        for i in np.arange(gt.min(), gt.max(), delta):
            gt_mask = np.ma.masked_outside(gt, i, i + delta)
            pred_mask = np.ma.masked_array(out, gt_mask.mask)

            flatten_range_gt = np.ma.compressed(gt_mask)
            flatten_range_pred = np.ma.compressed(pred_mask)

            if len(flatten_range_gt) > 0 and len(flatten_range_gt) > 0:
                rmse = np.sqrt(mse(flatten_range_gt, flatten_range_pred))
                index.append(i)
                errors_by_range.append(rmse)

        labels = list(zip(index, errors_by_range))
        plt.plot(index, errors_by_range)
        
        # st()

    def calculate_all(self):
        abs_rel = 0
        abs_mean = 0
        sq_rel = 0
        msee = 0
        rmse = 0
        # ssime = 0
        rmse_log = 0
        abs_log = 0
        acc1 = 0
        acc2 = 0
        acc3 = 0
        if 'ISPRS' in self.opt.path_gt:
            for gt_np, out_np in tqdm(self.lists):
                # sns.distplot(gt_np)
                # plt.figure()
                # sns.distplot(out_np)
                # plt.show()
                # st()
                # Classical evaluation measurements
                gt_np = gt_np[0]
                out_np = out_np[0]
                abs_mean += abse(gt_np, out_np)
                msee += mse(gt_np, out_np)
                rmse += np.sqrt(mse(gt_np, out_np))
        else:
            for (gt_np, mask), out_path in tqdm(self.lists):
                out_np = (load_and_mask_depth(out_path, mask)).astype(gt_np.dtype)
                if self.opt.normalize:
                    gt_np, out_np = self.normalize_raster(gt_np, out_np, min_v=self.min_v, max_v=self.max_v)

                gt_np, out_np = mask_invalid_depth_bigger_then(gt_np, out_np)
                # sns.distplot(gt_np)
                # plt.figure()
                # sns.distplot(out_np)
                # plt.show()
                # st()
                # Classical evaluation measurements
                abs_mean += abse(gt_np, out_np)
                msee += mse(gt_np, out_np)
                # abs_rel += abse(gt_np / gt_np, out_np / gt_np)
                # sq_rel += np.mean(((gt_np - out_np) ** 2) / gt_np)
                rmse += np.sqrt(mse(gt_np, out_np))
                # # ssime += ssim(gt_np, out_np)
                # rmse_log += np.sqrt(mse(np.log(gt_np), np.log(out_np)))
                # abs_log += abse(np.log10(gt_np), np.log10(out_np))
                # thres = np.maximum((gt_np / out_np), (out_np / gt_np))
                # acc1 += (thres < 1.25).mean()
                # acc2 += (thres < 1.25 * 2).mean()
                # acc3 += (thres < 1.25 * 3).mean()
                # self.plot(gt_np, out_np)
                # KITTI error Measurements
        plt.show()
        n_images = len(self.lists)
        # abs_rel  /= n_images
        # sq_rel   /= n_images
        abs_mean /= n_images
        msee      /= n_images
        rmse     /= n_images
        # ssime    /= n_images
        # rmse_log /= n_images
        # abs_log   /= n_images
        # acc1     /= n_images
        # acc2     /= n_images
        # acc3     /= n_images
        # print('Cap: {}'.format(self.opt.max_depth_mask))
        print('Name: {}', format(self.opt.name))
        # print('ABS_REL: {:.3f}'.format(abs_rel))
        # print('SQ_REL: {:.3f}'.format(sq_rel))
        print('ABS_Mean: {:.3f}'.format(abs_mean))
        print('MSE: {:.3f}'.format(msee))
        print('RMSE: {:.3f}'.format(rmse))
        # print('SSIM: {:.3f}'.format(ssime))
        # print('RMSE_log: {:.3f}'.format(rmse_log))
        # print('ABS_log: {:.3f}'.format(abs_log))
        # print('ACC1: {:.1f}\%'.format(acc1 * 100))
        # print('ACC2: {:.1f}\%'.format(acc2 * 100))
        # print('ACC3: {:.1f}\%'.format(acc3 * 100))
        # print('& {:.3f} & {:.3f} & {:.3f} && {:.1f}\\% & {:.1f}\\% & {:.1f}\\%'.format(abs_rel, rmse, rmse_log, acc1 * 100, acc2 * 100, acc3 * 100))
        print('& {:.3f} & {:.3f} & {:.3f}'.format(abs_mean, msee, rmse))

        # Kitti evaluation

