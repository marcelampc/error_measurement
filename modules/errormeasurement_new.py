# No concatenation
from PIL import Image
import numpy as np
import glob
from ipdb import set_trace as st
from tqdm import tqdm
from os.path import join

from sklearn.metrics import mean_squared_error as mse
from sklearn.metrics import mean_absolute_error as abse

size = 0

def crop_kitti_supervised(img):
        # resize because idk why dataset images do not have the same resolution
        # img = img.resize((1242, 375), Image.NEAREST)
        # crop 1/3 of the image like Eigen et al.
        height_start = img.size[1] // 3.2  # did this to have an image with height > 256
        img = img.crop((0,
                        height_start,
                        img.size[0],
                        img.size[1]
                        ))
        return img

def load_depth(path, gt_image=False):
    # size = (1242, 375)
    global size
    if gt_image == True:
        img_pil = Image.open(path)
        # img_np = np.array(crop_kitti_supervised(Image.open(path)))
        img_np = np.array(Image.open(path))
        size = (img_np.shape[1], img_np.shape[0])
    else: 
        interpolation = Image.BILINEAR
        img_np = np.array(Image.open(path).resize(size, interpolation))
    return img_np

def mask_invalid_pixels_depth(gt, pred, max_depth_mask):
    gt = np.ma.masked_greater_equal(gt, max_depth_mask)
    pred = np.ma.masked_array(pred, gt.mask)

    gt = np.ma.compressed(gt)
    pred = np.ma.compressed(pred)

    gt = np.ma.masked_equal(gt, 0.0)
    pred = np.ma.masked_array(pred, gt.mask)

    gt = np.ma.compressed(gt)
    pred = np.ma.compressed(pred)

    return gt, pred

class ErrorMeasure():
    def __init__(self, opt):
        self.opt = opt

    def initialize(self):
        self.lists = self.get_filenames( self.opt.which_dataset, self.opt.phase, self.opt.path_gt, self.opt.path_pred)

    def get_filenames(self, dataset_name, phase, target_path, out_path):
        if dataset_name == 'nyu':
            from modules.dataset.dataset_bank import dataset_nyu
            output_files_list = sorted(glob.glob(join(out_path, '*.png')))
            return list(zip(dataset_nyu(target_path, phase), output_files_list))
        elif dataset_name == 'kitti':
            from modules.dataset.dataset_bank import dataset_kitti
            output_files_list = sorted(glob.glob(join(out_path, '*.png')))
            target_files_list = dataset_kitti(target_path, phase, self.opt)
            # target_files_list = sorted(glob.glob(join(target_path, '*.png'))) # to test using target folder in results
            return list(zip(target_files_list, output_files_list))

    def calculate_all(self):
        abs_rel = 0
        sq_rel = 0
        rmse = 0
        rmse_log = 0
        abs_log = 0
        acc1 = 0
        acc2 = 0
        acc3 = 0
        for gt_path, out_path in tqdm(self.lists):
            gt_np = load_depth(gt_path, gt_image=True) / 1000 # / self.opt.scale_to_mm
            out_np = load_depth(out_path) *10 / 65535 # / self.opt.scale_to_mm
            if self.opt.extra_mask:
                gt_np, out_np = mask_invalid_pixels_depth(gt_np, out_np, self.opt.max_depth_mask)

            # Classical evaluation measurements
            # abs_rel += abse(gt_np / gt_np, out_np / gt_np)
            # sq_rel += np.mean(((gt_np - out_np) ** 2) / gt_np)
            rmse += np.sqrt(mse(gt_np, out_np))
            # rmse_log += np.sqrt(mse(np.log(gt_np), np.log(out_np)))
            # abs_log += abse(np.log10(gt_np), np.log10(out_np))
            thres = np.maximum((gt_np / out_np), (out_np / gt_np))
            acc1 += (thres < 1.25).mean()
            # acc2 += (thres < 1.25 * 2).mean()
            # acc3 += (thres < 1.25 * 3).mean()

            # KITTI error Measurements

        n_images = len(self.lists)
        # abs_rel  /= n_images
        # sq_rel   /= n_images
        rmse     /= n_images
        # rmse_log /= n_images
        # abs_log   /= n_images
        acc1     /= n_images
        # acc2     /= n_images
        # acc3     /= n_images
        # print('Cap: {}'.format(self.opt.max_depth_mask))
        # print('ABS_REL: {:.3f}'.format(abs_rel))
        # print('SQ_REL: {:.3f}'.format(sq_rel))
        print('Name: {}', format(self.opt.name))
        print('RMSE: {:.3f}'.format(rmse))
        # print('RMSE_log: {:.3f}'.format(rmse_log))
        # print('ABS_log: {:.3f}'.format(abs_log))
        print('ACC1: {:.1f}\%'.format(acc1 * 100))
        print('ACC2: {:.1f}\%'.format(acc2 * 100))
        print('ACC3: {:.1f}\%'.format(acc3 * 100))
        # print('& {:.3f} & {:.3f} & {:.3f} && {:.1f}\\% & {:.1f}\\% & {:.1f}\\%'.format(abs_rel, rmse, rmse_log, acc1 * 100, acc2 * 100, acc3 * 100))

        # Kitti evaluation

