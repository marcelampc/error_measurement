# By Alexandre Boulch
import numpy as np
import os
import argparse
from tqdm import tqdm
from PIL import Image

parser = argparse.ArgumentParser()
parser.add_argument("--dataset_root", type=str, default=".")
parser.add_argument("--results_name", type=str, default="name")
parser.add_argument("--targetdir", type=str, default=".")
parser.add_argument("--depthdir", type=str, default=".")
parser.add_argument("--testfile", type=str, default=".txt")
parser.add_argument("--testsplit", type=str, default="val1", help='val1, val2, test')
parser.add_argument("--epoch", default="latest")
parser.add_argument("--suffix", type=str, default=None)
parser.add_argument("--saveCurves", type=str, default=None, help="name to save the curves")
parser.add_argument("--savedir", type=str, default=".")
args = parser.parse_args()
from ipdb import set_trace as st

def get_names_from_dir():
    import glob
    args.dataset_root = 'datasets/Challenge2018'
    l_depths = sorted(glob.glob(os.path.join('./results/test_results', args.results_name, args.epoch, 'output', '*.png')))
    if '1' in args.testsplit:
        index = '[0][0][0][1]'
    elif '2' in args.testsplit:
        index = '[0][2][2][4]'
    phase_path = os.path.join(args.dataset_root, 'training')
    vcam_ids = '*[02468]'
    scene_folders = '*{}'.format(index)
    l_target_depths = sorted(glob.glob(os.path.join(phase_path, scene_folders, vcam_ids, '*.bin')))
    test_filenames = [(l_depth, l_target_depth) for (l_depth, l_target_depth) in zip(l_depths, l_target_depths)]
    print("Number of testing files", len(test_filenames))
    return test_filenames

def get_names_from_file():
    test_filenames = []
    f = open(args.testfile,"r")
    for line in f:
        l_image = line.split("\n")[0]

        l_depth = l_image.replace("undist", "dmap")
        l_target_depth = l_depth.replace(".png", ".bin")

        if args.suffix is not None:
            l_depth = l_depth.replace("dmap", args.suffix)

        l_depth = os.path.join(args.depthdir, l_depth)

        l_target_depth = os.path.join(args.targetdir, l_target_depth)

        test_filenames.append([l_depth, l_target_depth])
    f.close()
    print("Number of testing files", len(test_filenames))
    return test_filenames

def error_abs(im1, im2): # im1 is target
    div = im1.copy()
    div[div==0] = 1
    return (np.abs(im1-im2)/div).mean()

def error_mean_log10(im1, im2):
    d = np.abs(np.log10(1+im1)-np.log10(1+im2))
    return d.mean()

def error_rms(im1, im2):
    return np.sqrt(((im1-im2)**2).mean())

def error_log_rms(im1, im2):
    return np.sqrt(((np.log(1+im1)-np.log(1+im2))**2).mean())


def accuracy_threshold(im1, im2, threshold):
    maxi = np.maximum((1+im1)/(1+im2), (1+im2)/1+im1)
    number_th = (maxi<threshold).sum()
    number = im1.shape[0] * im1.shape[1]
    return number_th / number

def histogram(im1, im2, bins):
    values = np.abs(im1-im2)
    hist, _ = np.histogram(values, bins=bins)
    return hist

def image_loader_depth(image_path):
    """Load an image.
        Args:
            image_path
        Returns:
            A numpy float32 array shape (w,h, n_channel)
    """
    imsize=(640,480)
    im = Image.open(image_path)
    # im = im.resize(imsize, Image.NEAREST)
    im = np.array(im, dtype=np.float32)/1000 #in meters
    im = im.reshape(im.shape+(1,))
    return im


imsize_orig = (480,640)
def target_loader_depth(image_path):
    """Load a target image.
        Args:
            image_path
        Returns:
            A numpy matrix shape (w,h, n_channels) (if multi channels), (w,h) otherwise
            int64 if discrete label
    """
    if image_path is None:
        return None
    
    # load bin file from 3DRMS
    target = np.fromfile(image_path, dtype='>f')
    ims = (imsize_orig[1], imsize_orig[0])
    target = target.reshape(ims).transpose()
    
    target = target.reshape(target.shape+(1,))

    return target.astype(np.float32).copy()

test_filenames = get_names_from_dir() # get_names_from_file()

err_abs = 0
err_mean_log = 0
err_rms = 0
err_log_rms = 0


threshold_1_25_p1 = 0
threshold_1_25_p2 = 0
threshold_1_25_p3 = 0

histo = None
bins = np.arange(0.5,15.5,0.1)

t = enumerate(tqdm(test_filenames))
for fnames_id, fnames in t:
    depth = image_loader_depth(fnames[0])
    target = target_loader_depth(fnames[1])
    # print('depth: {} and target {}'.format(fnames[0], fnames[1]))

    err_abs += error_abs(target, depth)
    err_rms += error_rms(target, depth)
    err_mean_log += error_mean_log10(target, depth)
    err_log_rms += error_log_rms(target, depth)

    threshold_1_25_p1 += accuracy_threshold(target, depth, 1.25)
    threshold_1_25_p2 += accuracy_threshold(target, depth, 1.25**2)
    threshold_1_25_p3 += accuracy_threshold(target, depth, 1.25**3)

    hist = histogram(target, depth, bins)

    if histo is None:
        histo = hist
    else:
        hist += hist

    # if fnames_id > 100:
    #     break

if args.saveCurves is not None:
    arr = np.zeros((histo.shape[0]+1, 3))
    arr[0,:] = 0
    arr[1:,0] = bins[:-1]
    arr[1:,1] = histo / histo.sum()
    arr[1:,2] = np.cumsum(histo)/histo.sum()
    np.savetxt(os.path.join(args.savedir, args.saveCurves), arr)


print("err_abs", err_abs/len(test_filenames))
print("err_mean_log", err_mean_log/len(test_filenames))
print("err_rms", err_rms/len(test_filenames))
print("err_log_rms", err_log_rms/len(test_filenames))
print("acc 1.25**1", threshold_1_25_p1 / len(test_filenames))
print("acc 1.25**2", threshold_1_25_p2 / len(test_filenames))
print("acc 1.25**3", threshold_1_25_p3 / len(test_filenames))

print('\n& {:.3f} & {:.3f} & {:.3f} & {:.3f} && {:.1f}\\% & {:.1f}\\% & {:.1f}\\%\n'.format(err_abs/len(test_filenames), err_mean_log/len(test_filenames), err_rms/len(test_filenames), err_log_rms/len(test_filenames), (threshold_1_25_p1 / len(test_filenames))*100, (threshold_1_25_p2 / len(test_filenames))*100, (threshold_1_25_p3 / len(test_filenames))*100))
            
