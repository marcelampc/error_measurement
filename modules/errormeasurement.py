#!/usr/bin/python3
###
# Error Measurements between 2 images 1 or 3 channels
# 3 channels not implemented
# Marcela Carvalho
###

#from skimage.measure import structural_similarity as ssim
import matplotlib.pyplot as plt
import numpy as np
import os
from os.path import join
from skimage import img_as_float, io
import argparse
import sys
from ipdb import set_trace as st
from sklearn.metrics import mean_squared_error as mse
from sklearn.metrics import mean_absolute_error as abse
from skimage.measure import compare_ssim as ssim
import operator
from scipy.misc import imresize
import cv2
from PIL import Image

from ipdb import set_trace as st

THRES = 1.25**1
THRES_2 = 1.25**2
THRES_3 = 1.25**3
class ErrorMeasure():
    def __init__(self, opt=None):
        if opt != None:
            self.opt = opt
            self.avre = -1
            self.sqre = -1
            self.rmse = -1
            self.logrmse = -1
            self.log10 = -1
            self.accuracy_threshold = []
            self.ranking_list = []


    def unit8_to_meters(self, img):
        """ Convert pixel values to distance in meters
        """
        min_value = 0
        if self.opt.which_dataset == 'nyu':
            max_value = 10
        elif self.opt.which_dataset == 'make3d':
            max_value = 81
        elif self.opt.which_dataset == 'kitti':
            max_value = float(1 / 256)
        # values between [min max] given by user
        #print('min: {} max: {}'.format(img.min(), img.max()))
        norm_image = img * (max_value - min_value) + min_value

        return norm_image

    def mask_invalid_pixels_depth(self, gt_conc, pred_conc):
        # self.gt_conc, self.pred_conc
        gt_conc = np.ma.masked_greater_equal(gt_conc, self.opt.max_depth_mask)
        pred_conc = np.ma.masked_array(pred_conc, gt_conc.mask)

        gt_conc = np.ma.compressed(gt_conc)
        pred_conc = np.ma.compressed(pred_conc)

        gt_conc = np.ma.masked_equal(gt_conc, 0.0)
        pred_conc = np.ma.masked_array(pred_conc, gt_conc.mask)

        gt_conc = np.ma.compressed(gt_conc)
        pred_conc = np.ma.compressed(pred_conc)

        return gt_conc, pred_conc

    def apply_mask(self, gt_conc, pred_conc):
        gt_conc = np.ma.masked_equal(gt_conc, 0.0)
        pred_conc = np.ma.masked_array(pred_conc, gt_conc.mask)

        gt_conc = np.ma.compressed(gt_conc)
        pred_conc = np.ma.compressed(pred_conc)

        return gt_conc, pred_conc

    def convert_in_meters_concatenate(self):
        """
        """
        gt_conc = np.concatenate([self.unit8_to_meters(gt) for (name, gt) in self.gt_list])
        pred_conc = np.concatenate([self.unit8_to_meters(pred) for pred in self.pred_list])

        assert(gt_conc.shape == pred_conc.shape), "Given folders doesnt have the same number of files"
        return gt_conc, pred_conc

    def get_scale_img_type(self, path):
        if Image.open(join(path, os.listdir(path)[0])).mode == 'I':
            return 65535.0 #1000 # 65535.0
        else:
            return 255.0

    def initialize(self):
        if self.opt.data_root is not None:
            self.opt.dataroot = os.path.join(self.opt.data_root + 'results', self.opt.name)
            epoch_folder = self.opt.epoch + '_net_G_' + self.opt.phase
            self.opt.dataroot = os.path.join(self.opt.dataroot, epoch_folder)
            self.opt.path_gt = os.path.join(self.opt.dataroot, 'images/target')
            self.opt.path_pred = os.path.join(self.opt.dataroot, 'images/output')

        path_gt = self.opt.path_gt
        path_pred = self.opt.path_pred

        if(self.opt.original_resolution):
            # original images come with values between 0-255, so we have to rescale
            # for interpolation, use either cv2.INTER_CUBIC (slow) or cv2.INTER_LINEAR
            self.gt_list = []
            scale_gt = self.get_scale_img_type(path_gt)
            scale_pred = self.get_scale_img_type(path_pred)

            if self.opt.which_dataset == 'kitti':
                for f in sorted(os.listdir(path_gt)):
                    w, h = (1242, 375)
                    self.gt_list.append([f, (np.array(Image.open(join(path_gt, f)).resize((w, h), Image.BILINEAR), dtype=np.float))])
                    self.pred_list = [(np.array(Image.open(join(path_pred, f)).resize((w, h), Image.BILINEAR), dtype=np.float)) for f in sorted(os.listdir(path_pred))]
                    #self.gt_list.append([f, (np.array(Image.open(join(path_gt, f)), dtype=np.float))])
                    #self.pred_list = [(np.array(Image.open(join(path_pred, f)), dtype=np.float)) for f in sorted(os.listdir(path_pred))]
            else:
                for f in sorted(os.listdir(path_gt)):
                    self.gt_list.append([f, (np.array(Image.open(join(path_gt, f)), dtype=np.float) / scale_gt)])
                h, w = self.gt_list[0][1].shape
                print('x: {}  y={}'.format(w, h))
                self.pred_list = [(np.array(Image.open(join(path_pred, f)).resize((w, h), Image.BICUBIC), dtype=np.float) / scale_pred) for f in sorted(os.listdir(path_pred))]
        else:
            self.gt_list = [[f, img_as_float(io.imread(os.path.join(path_gt, f), as_grey=True))] for f in sorted(os.listdir(path_gt))]
            self.pred_list = [img_as_float(io.imread(os.path.join(path_pred, f), as_grey=True)) for f in sorted(os.listdir(path_pred))]

        # check if dimensions are the same
        assert(self.gt_list[0][1].shape == self.pred_list[0].shape), "Given folders doesnt contain images with same resolutions {} and {}. Use option --original_resolution.".format(self.gt_list[0][1].shape, self.pred_list[0].shape)

        # convert to meters
        [self.gt_conc, self.pred_conc] = self.convert_in_meters_concatenate()

        if self.opt.extra_mask:
            print("C1 error with depth mask from {} meters.".format(self.opt.max_depth_mask))
            [self.gt_conc, self.pred_conc] = self.mask_invalid_pixels_depth(self.gt_conc, self.pred_conc)

    def rmse_(self, img1, img2):
        """ Root Mean Squared Error
        dim(img1) must be = dim(img2)
        returns mse
        """
        return np.sqrt(mse(img1, img2))

    def calculate_rmse(self):
        self.rmse = self.rmse_(self.gt_conc, self.pred_conc)
        return self.rmse

    def logrmse_(self, img1, img2):
        if((img1 == 0).any() or (img2 == 0).any()):
            return 0
        else:
            # err = np.sum((np.log(img1) - np.log(img2)) ** 2)
            # err /= float(img1.shape[0] * img1.shape[1])  # divide by number of elements
            # return np.sqrt(err)
            return np.sqrt(mse(np.log(img1), np.log(img2)))

    def calculate_logrmse(self):
        self.logrmse = self.logrmse_(self.gt_conc, self.pred_conc)
        return self.logrmse

    def avre_(self, img1, img2):
        """ Average Relative Error
        Args:
            img1, img2: numpy.ndarrays

        Returns:
            float
        """
        return abse(np.divide(img1, img1), np.divide(img2, img1))

    def calculate_avre(self):
        self.avre = self.avre_(self.gt_conc, self.pred_conc)
        return self.avre

    def sqre_(self, img1, img2):
        return rme(img1, img2)

    def calculate_sqre(self):
        self.sqre = self.sqre_(self.gt_conc, self.pred_conc)
        return self.sqre

    def log10_(self, img1, img2):
        """ Average Log10 error
        """
        if((img1 == 0).any() or (img2 == 0).any()):
            return 0
        else:
            return abse(np.log10(img1), np.log10(img2))
	#except:
        #    return 0

    def calculate_log10(self):
        self.log10 = self.log10_(self.gt_conc, self.pred_conc)
        return self.log10

    def calculate_ssim(self):
        error = ssim(self.gt_conc, self.pred_conc)
        return error

    def accuracy_threshold_(self, img1, img2, thres):
        """ Accuracy with threshold
        Args:

        Returns:

        """
        # print img1
        # just to be sure it's float, it's not necessary if it's in meters
        # c was added to avoid dividing by 0
        np.seterr(divide='ignore')
        div1 = np.divide(img1, img2)
        div2 = np.divide(img2, img1)

        # gets maximun value element wise between 2 arrays
        max_array = np.maximum(div1, div2)
        # generate boolean array. Send true to values < threshold
        compare_to_thres_array = np.less(max_array, thres)

        # count true values
        try:
            perc_values = float(np.sum(compare_to_thres_array) / float(img1.shape[0] * img1.shape[1]))
        except:
            perc_values = float(np.sum(compare_to_thres_array) / float(img1.shape[0]))
        return perc_values

    # def calculate_accuracy_threshold(self):
    #     self.accuracy_threshold.append()

    def individual_metrics(self, name, gt, pred, thres):
        # Individual information
        # save to text file

        print('Image name: {}'.format(name))

        print('Average relative error: {:4}'.format(self.avre_(gt,pred)))
        # print('Squared relative error: {:4}'.format(self.sqre_(gt,pred)))
        print('Root Mean Squared (lin) error: {:4}'.format(self.rmse_(gt, pred)))
        print('Root Mean Squared (log) error: {:4}'.format(self.logrmse_(gt, pred)))
        # print('Average log10 error: {:4}'.format(self.log10(gt,pred)))
        print('Accuracy with 1.25: {:.2%}'.format(self.accuracy_threshold_(gt, pred, THRES)))
        print('Accuracy with 1.25**2: {:.2%}'.format(self.accuracy_threshold_(gt, pred, THRES_2)))
        print('Accuracy with 1.25**3: {:.2%}'.format(self.accuracy_threshold_(gt, pred, THRES_3)))
        print('\n')

    def create_ranking_list(self, method='accuracy'):
        # create list then sort
        self.ranking_list = dict([[name, self.accuracy_threshold_(self.unit8_to_meters(gt), self.unit8_to_meters(pred), THRES)] for (name, gt), pred in zip(self.gt_list, self.pred_list)])
        self.ranking_list = sorted(self.ranking_list.items(), key=operator.itemgetter(1), reverse=True)

    def get_ranking_list(self):
        if len(self.ranking_list) == 0:
            self.create_ranking_list()
        return self.ranking_list

    def calculate_per_iter(self):
        # if opt.each_iter:
        counter_bad = 0
        counter_good = 0
        for (name, gt), pred in zip(self.gt_list, self.pred_list):
            gt_meters = self.unit8_to_meters(gt)
            pred_meters = self.unit8_to_meters(pred)
            self.individual_metrics(name, gt_meters, pred_meters, THRES)
        #     print('\n')
        #     if self.accuracy_threshold_(gt_meters, pred_meters, THRES) <= self.opt.down:
        #         self.individual_metrics(name, gt_meters, pred_meters, THRES)
        #         print('\n')
        #         counter_bad=counter_bad+1
        #     if self.accuracy_threshold_(gt_meters, pred_meters, THRES) > self.opt.up:
        #         self.individual_metrics(name, gt_meters, pred_meters, THRES)
        #         print('\n')
        #         counter_good=counter_good+1

        # print("number of terrible predictions: {}".format(counter_bad))
        print("number of good predictions: {}".format(counter_good))

    def calculate_make3D(self):
            print('TOTAL DATASET MEASUREMENT ERROR NAME: {} EPOCH: {}'.format(self.opt.name, self.opt.epoch))
            print('Mean Relative Error:\t{:4}'.format(self.avre if self.avre > 0 else self.calculate_avre()))
            print('Mean log10 Error:\t{:4}'.format(self.log10 if self.log10 > 0 else self.calculate_log10()))
        #print(''.format(self.))
            #print('Squared relative error: {:4}'.format(self.sqre if self.sqre > 0 else self.calculate_sqre()))
            print('Root Mean Squared (lin) error:\t{:4}'.format(self.rmse if self.rmse > 0 else self.calculate_rmse()))
            print('\n')
            print('& {:.3f} & {:.3f} & {:.3f}'.format(self.avre, self.log10, self.rmse))
            print('\n')
            print('\n\n')

    def calculate_nyu(self):
            print('TOTAL DATASET MEASUREMENT ERROR NAME: {} EPOCH: {}'.format(self.opt.name, self.opt.epoch))
            print('Mean Relative Error:\t{:4}'.format(self.avre if self.avre > 0 else self.calculate_avre()))
            print('Mean log10 Error:\t{:4}'.format(self.log10 if self.log10 > 0 else self.calculate_log10()))
        #print(''.format(self.))
            #print('Squared relative error: {:4}'.format(self.sqre if self.sqre > 0 else self.calculate_sqre()))
            print('Root Mean Squared (lin) error:\t{:4}'.format(self.rmse if self.rmse > 0 else self.calculate_rmse()))
            print('Root Mean Squared (log) error:\t{:4}'.format(self.logrmse if self.logrmse > 0 else self.calculate_logrmse()))
            self.accuracy_threshold.append(self.accuracy_threshold_(self.gt_conc, self.pred_conc, THRES))
            print('Accuracy with 1.25:\t{:.2%}'.format(self.accuracy_threshold[0]))
            self.accuracy_threshold.append(self.accuracy_threshold_(self.gt_conc, self.pred_conc, THRES_2))
            print('Accuracy with 1.25**2:\t{:.2%}'.format(self.accuracy_threshold[1]))
            self.accuracy_threshold.append(self.accuracy_threshold_(self.gt_conc, self.pred_conc, THRES_3))
            print('Accuracy with 1.25**3:\t{:.2%}'.format(self.accuracy_threshold[2]))
            # print('SSIM: {:4}'.format(self.calculate_ssim()))
            print('\n')
            print('& {:.3f} & {:.3f} & {:.3f} & {:.3f} && {:.1f}\\% & {:.1f}\\% & {:.1f}\\%'.format(self.avre, self.log10, self.rmse, self.logrmse, self.accuracy_threshold[0]*100, self.accuracy_threshold[1]*100, self.accuracy_threshold[2]*100))
            print('\n')
            print('\n\n')

    def calculate_all(self):
        if self.opt.which_dataset == 'nyu' or self.opt.which_dataset == 'kitti':
            self.calculate_nyu()
        else:
            self.calculate_make3D()


    def calculate_all_mask():
        """
        Perform all measurements using a mask for invalid pixels
        """
        # create a dictionary to print better
