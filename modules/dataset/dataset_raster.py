# based on https://github.com/pytorch/examples/blob/master/super_resolution/
# dataset.py
# Instead of applying data augmentation to PIL.Image, apply to numpy (from AB and NA)
# custom dataset to store 2 images

# Expects that files are like:
# |- main_folder
# |- |- train
# |- |- |- gt
# |- |- |- label
# |- |- test
# |- |- |- gt
# |- |- |- label
# |- |- val
# |- |- |- gt
# |- |- |- label

import torch.utils.data as data

import os
from os import listdir
from os.path import join
from PIL import Image
from ipdb import set_trace as st
import random
from math import pow
import numpy as np
from tqdm import tqdm
from random import randint

import torch

from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True

# global variable to apply crop
state = 0

def get_paths_list(opt, phase):
    if opt.dataset_name == 'dfc':
        from .dataset_bank import dataset_dfc
        return dataset_dfc(opt.dataroot, phase)

def get_random_pos(img, window_shape):
    """ Extract of 2D random patch of shape window_shape in the image """
    w, h = window_shape
    W, H = img.shape[-2:]#[::-1]
    x1 = random.randint(0, W - w - 1)
    x2 = x1 + w
    y1 = random.randint(0, H - h - 1)
    y2 = y1 + h
    return x1, x2, y1, y2

def sliding_window(image, step=10, window_size=(20,20)):
    """ Slide a window_shape window across the image with a stride of step """
    step = step if len(step) == 2 else step * 2 
    height, width = image.shape if len(image.shape) == 2 else (image.shape[1], image.shape[2])
    for x in range(0, width, step[0]):
        if x + window_size[0] > width:
            x = width - window_size[0]
        for y in range(0, height, step[1]):
            if y + window_size[1] > height:
                y = height - window_size[1]
            yield x, x + window_size[0], y, y + window_size[1]

def str2bool(values):
    return [v.lower() in ("true", "t") for v in values]

def normalize_raster_to_numpy(data):
    return np.asarray(((data.transpose(1,2,0) + 1) / 2) * 255).astype('uint8')

def numpy_to_normalized_raster(data):
    return (((data.astype('float32') / 255.0) * 2.0) - 1.0).transpose(2,0,1)

def getFeatures(gdf):
    """Function to parse features from GeoDataFrame in such a manner that rasterio wants them"""
    import json
    return [json.loads(gdf.to_json())['features'][0]['geometry']]

def resize_rgb(data, image_size, mode):
    img = normalize_raster_to_numpy(data)
    img = resize(img, image_size, mode)
    img = numpy_to_normalized_raster(img)
    return img

def resize(data, image_size, mode):
    return np.array(Image.fromarray(data).resize(image_size, mode))

def load_depths_and_masks(input_list, target_path):
    """
    load all depths/labels to label_cache with crop
    """
    import rasterio
    from rasterio.mask import mask
    from rasterio.plot import reshape_as_image
    import geopandas as gpd
    from shapely.geometry import box
    from scipy.misc import imresize

    # depth_image_raster = rasterio.open(target_path[0])
    depth_raster = [rasterio.open(path) for path in target_path]

    for i, img_input_path in enumerate(tqdm(input_list)):
        with rasterio.open(img_input_path) as raster_rgb:
            # get bounds and crop from depth_image_raster
            bounds = raster_rgb.bounds
            bbox = box(bounds.left, bounds.bottom, bounds.right, bounds.top)
            geo = gpd.GeoDataFrame({'geometry': bbox}, index=[0])
            coords = getFeatures(geo)
            depth_patchs_transforms = [mask(image_raster, shapes=coords, crop=True) for image_raster in depth_raster]

            depth_patch, depth_patch_transform = depth_patchs_transforms[0]

            if len(depth_patchs_transforms) == 2:
                depth_patch_dsm = depth_patch
                depth_patch_dem = depth_patchs_transforms[1][0]
                mask_dsm = (depth_patch_dsm < 9000) * (depth_patch_dem < 9000)
                not_mask_dsm = np.logical_not(mask_dsm)
                depth_patch = mask_dsm * (depth_patch_dsm - depth_patch_dem)
                depth_patch = depth_patch * ((not_mask_dsm * 10000) + 1)

            yield mask_invalid_depth(depth_patch)

def mask_invalid_depth(depth, offset=0.0, threshold=9000.0):
    import numpy as np
    depth = depth + offset # bigger than the minimum 
    # mask numbers > depth_max
    # depth_mask = depth < (9000) # everest has 8.848m
    mask = np.ma.masked_greater(depth, threshold)
    return np.ma.compressed(np.ma.masked_array(depth, mask.mask)), mask

def load_raster(target_list, phase='train'):
    for path_output in tqdm(target_list):
        target = load_labels_raster(path_output)
        yield target
        
def load_raster_multitask(target_list, phase='train'):
    import rasterio, os
    image_list = []
    for path_output in tqdm(target_list):
        target = load_labels_raster(path_output, path_output2)
        yield raster_rgb_norm, target

def load_labels_raster(*path_list):
    import rasterio
    from rasterio.profiles import DefaultGTiffProfile
    target_np = []
    for path in path_list:
        # target = rasterio.open(path).read()
        target = np.array(Image.open(path))
        if target.dtype == 'uint8' and len(target.shape) == 3:
            from util.util import colors_to_labels
            colors_isprs = [ 
                            [0, 0, 0],
                            [255, 255, 255],
                            [0, 0, 255],
                            [0, 255, 255],
                            [0, 255, 0],
                            [255, 255, 0],
                            [255, 0, 0],
                            ]
            # target = colors_to_labels(target.transpose(1,2,0), colors_isprs)
            target = colors_to_labels(target, colors_isprs)
        else: 
            if target.dtype == 'uint8' and target.max() > 8: # height
                # values between 0 and 1
                target = target.astype(np.float32) / 255
        target_np.append(target)
    return target_np