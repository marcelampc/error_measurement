from os.path import join
import glob
from ipdb import set_trace as st

##### DATASET CITYSCAPES #####

# Not needed for inference metrics
# def dataset_cityscapes_get_rgb(root, data_split):
#     path = join(root, 'leftImg8bit', data_split)
#     city = '*'
#     return sorted(glob.glob(join(path, city, '*.png')))

def dataset_cityscapes_get_disparity(root, data_split):
    path = join(root, 'disparity', data_split)
    city = '*'
    return sorted(glob.glob(join(path, city, '*.png')))

def dataset_cityscapes_get_instance(root, data_split):
    path = join(root, 'gtFine_trainvaltest/gtFine', data_split)
    city = '*'
    return sorted(glob.glob(join(path, city, '*instanceIds.png')))

def dataset_cityscapes_get_semantics(root, data_split):
    path = join(root, 'gtFine_trainvaltest/gtFine', data_split)
    city = '*'
    return sorted(glob.glob(join(path, city, '*labelIds.png')))

def dataset_cityscapes(root, data_split, tasks, n_files=None):
    # rgb_images = dataset_cityscapes_get_rgb(root, data_split)
    tasks_list = []
    for task in tasks:
        if 'semantics' in task:
            tasks_list.append(dataset_cityscapes_get_semantics(root, data_split)[:n_files])
        if 'regression' in task:
            tasks_list.append(dataset_cityscapes_get_disparity(root, data_split)[:n_files])
        if 'instance' in task:
            tasks_list.append(dataset_cityscapes_get_instance(root, data_split)[:n_files])
    return tasks_list
    
