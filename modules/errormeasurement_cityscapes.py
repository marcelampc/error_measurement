# No concatenation
from PIL import Image
import numpy as np
import glob
from ipdb import set_trace as st
from tqdm import tqdm
from os.path import join

from sklearn.metrics import mean_squared_error as mse
from sklearn.metrics import mean_absolute_error as abse

size = 0

def crop_kitti_supervised(img):
        # resize because idk why dataset images do not have the same resolution
        # img = img.resize((1242, 375), Image.NEAREST)
        # crop 1/3 of the image like Eigen et al.
        height_start = img.size[1] // 3.2  # did this to have an image with height > 256
        img = img.crop((0,
                        height_start,
                        img.size[0],
                        img.size[1]
                        ))
        return img

def load_data(path, dtype=np.float, gt_image=False, interpolation=Image.BILINEAR):
    # size = (1242, 375)
    global size
    if gt_image == True:
        img_pil = Image.open(path)
        img_np = np.array(Image.open(path), dtype=dtype)
        size = (img_np.shape[1], img_np.shape[0])
    else: 
        img_np = np.array(Image.open(path).resize(size, interpolation), dtype=dtype)
    return img_np

def mask_invalid_pixels_depth(gt, pred):
    gt = np.ma.masked_equal(gt, 0.0)
    pred = np.ma.masked_array(pred, gt.mask)

    gt = np.ma.compressed(gt)
    pred = np.ma.compressed(pred)

    return gt, pred

class ErrorMeasure():
    def __init__(self, opt):
        self.opt = opt

        self.void_classes = [0, 1, 2, 3, 4, 5, 6, 9, 10, 14, 15, 16, 18, 29, 30, -1]
        self.valid_classes = [7, 8, 11, 12, 13, 17, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 31, 32, 33]
        self.no_instances =  [7, 8, 11, 12, 13, 17, 19, 20, 21, 22, 23]

        self.ignore_index = 250
        self.ins_ignore_value = 250
        self.class_map = dict(zip(self.valid_classes, range(19)))

    def initialize(self):
        self.gt_list, self.pred_list = self.get_filenames( self.opt.dataset_name, self.opt.data_split, self.opt.path_gt, self.opt.path_pred, self.opt.tasks)

    def get_filenames(self, dataset_name, phase, target_path, out_path, tasks):
        from modules.dataset.dataset_bank_cityscapes import dataset_cityscapes
        predictions_list = [sorted(glob.glob(join(out_path, task, '*.png'))) for task in tasks]
        gt_list = dataset_cityscapes(root=target_path, data_split=self.opt.data_split, tasks=tasks, n_files=len(predictions_list[0]))
        assert(len(predictions_list[0])==len(gt_list[0]))
        return gt_list, predictions_list

    def calculate_all(self):
        for i, task in enumerate(self.opt.tasks):
            if task == 'semantics':
                self.calculate_all_semantics(gt_list=self.gt_list[i], pred_list=self.pred_list[i], n_classes=self.opt.outputs_nc[i])
            if task == 'regression':
                self.calculate_all_regression(gt_list=self.gt_list[i], pred_list=self.pred_list[i])
            if task == 'instance':
                pass
                # self.calculate_all_instance()

    def encode_segmap(self, mask):
        # Put all void classes to zero
        for _voidc in self.void_classes:
            mask[mask==_voidc] = self.ignore_index
        for _validc in self.valid_classes:
            mask[mask==_validc] = self.class_map[_validc]
        return mask

    def load_semantics(self, data, dtype=np.uint8, gt_true=False):
        global size
        if gt_true:
            image =  np.array(Image.open(data), dtype=dtype)
            size = image.size
            image_np = np.array(image, dtype=dtype)

    def calculate_all_semantics(self, gt_list, pred_list, n_classes):
        from sklearn.metrics import confusion_matrix
        from sklearn.metrics import f1_score
        import semseg.metrics.raster as metrics
        global_cm = 0
        for gt_filename, pred_filename in tqdm(zip(gt_list, pred_list)):
            # must encode segmap befor comparing
            gt_np = self.encode_segmap(load_data(gt_filename, dtype=np.uint8, gt_image=True))
            out_np = load_data(pred_filename, dtype=np.uint8, interpolation=Image.NEAREST)
            cm = confusion_matrix(gt_np.ravel(), out_np.ravel(), labels=list(range(0, n_classes)))
            global_cm += cm
            # st()

        # scores
        overall_acc = metrics.stats_overall_accuracy(global_cm)
        average_acc, _ = metrics.stats_accuracy_per_class(global_cm)
        average_iou, _ = metrics.stats_iou_per_class(global_cm)
        f1_score_avg, f1_score_per_class = metrics.stats_f1score_per_class(global_cm)
        # print('Cap: {}'.format(self.opt.max_depth_mask))
        print('Name: {}', format(self.opt.name))
        print('AF1: {:.1f}\\%'.format(f1_score_avg*100))
        print('OA: {:.1f}\\%'.format(overall_acc*100))
        print('AA: {:.1f}\\%'.format(average_acc*100))
        print('AIoU: {:.1f}\\%'.format(average_iou*100))

        print('& {:.1f}\\% & {:.1f}\\% & {:.1f}\\%'.format(overall_acc*100, average_acc*100, average_iou*100))

        print('\nF1 scores per class')
        string_f1_scores = ''
        for f1_score in f1_score_per_class:
            string_f1_scores += '& {:.1f}\\% '.format(f1_score*100)
        print(string_f1_scores)

    def calculate_all_regression(self, gt_list, pred_list):
        abs_rel = 0
        sq_rel = 0
        rmse = 0
        rmse_log = 0
        abs_log = 0
        acc1 = 0
        acc2 = 0
        acc3 = 0
        for gt_path, pred_path in tqdm(zip(gt_list, pred_list)):
            gt_np = load_data(gt_path, gt_image=True) / self.opt.scale
            pred_np = load_data(pred_path) / self.opt.scale
            # check depth values here
            gt_np, pred_np = mask_invalid_pixels_depth(gt_np, pred_np)
            # st()
            # ToDo: Add error for disparity

            # Classical evaluation measurements
            # abs_rel += abse(gt_np / gt_np, pred_np / gt_np)
            # sq_rel += np.mean(((gt_np - pred_np) ** 2) / gt_np)
            rmse += np.sqrt(mse(gt_np, pred_np))
            # rmse_log += np.sqrt(mse(np.log(gt_np), np.log(pred_np)))
            # abs_log += abse(np.log10(gt_np), np.log10(pred_np))
            thres = np.maximum((gt_np / pred_np), (pred_np / gt_np))
            acc1 += (thres < 1.25).mean()
            # acc2 += (thres < 1.25 * 2).mean()
            # acc3 += (thres < 1.25 * 3).mean()

            # KITTI error Measurements

        n_images = len(gt_list)
        # abs_rel  /= n_images
        # sq_rel   /= n_images
        rmse     /= n_images
        # rmse_log /= n_images
        # abs_log   /= n_images
        acc1     /= n_images
        # acc2     /= n_images
        # acc3     /= n_images
        # print('Cap: {}'.format(self.opt.max_depth_mask))
        # print('ABS_REL: {:.3f}'.format(abs_rel))
        # print('SQ_REL: {:.3f}'.format(sq_rel))
        print('Name: {}', format(self.opt.name))
        print('RMSE: {:.3f}'.format(rmse))
        # print('RMSE_log: {:.3f}'.format(rmse_log))
        # print('ABS_log: {:.3f}'.format(abs_log))
        print('ACC1: {:.1f}\%'.format(acc1 * 100))
        print('ACC2: {:.1f}\%'.format(acc2 * 100))
        print('ACC3: {:.1f}\%'.format(acc3 * 100))
        # print('& {:.3f} & {:.3f} & {:.3f} && {:.1f}\\% & {:.1f}\\% & {:.1f}\\%'.format(abs_rel, rmse, rmse_log, acc1 * 100, acc2 * 100, acc3 * 100))

        # Kitti evaluation

