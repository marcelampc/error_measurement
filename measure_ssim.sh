path_gt='../../../datasets/nyu_osplit/depth/test/'
name=$1
path_pred='../../cgan/results/test_results/'$name'/best/output/'

echo $name
python measureandsave.py --path_gt $path_gt --path_pred $path_pred --original_resolution
