root=/media/mcarvalh/DATASETS_MPC/2018/ECCV_3DRW
scale=1000.0
dataset=nyu_795_all_in_focus
name=denseUnet121basic__regression_L1__mb8_drop_pt_imagenet__$dataset
python ../scripts/error_measurement/measureandsave.py --path_gt $root/datasets/$dataset --path_pred $root/results/$name --epoch $epoch --name $name --which_dataset nyu --original_resolution --scale_to_mm $scale >> ./error_$name.txt
