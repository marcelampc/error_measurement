# Error measurement and saving image

### Prerequisites
Python libraries: numpy, skimage, opencv, sklearn, ipdb, matplotlib .

## Measure error

Compare images specifying GT and PRED folders if images have same resolution:
```
$ python measureandsave.py --path_gt /PATH/TO/GROUND/TRUTH --path_pred /PATH/TO/PREDICTION
```

If images doesn't have the same resolution, add the flag --original_resolution. It will resample the predicted image to the original resolution of the GT:
```
$ python measureandsave.py --path_gt /PATH/TO/GROUND/TRUTH --path_pred /PATH/TO/PREDICTION --original_resolution
```

If you want to test with the old (bad) test set:
```
$ python measureandsave.py --path_gt /PATH/TO/BAD_GROUND/TRUTH --path_pred /PATH/TO/PREDICTION --original_resolution --bad_minmax
```


Compare images using pix2pix output with ground truth at 256x256:
```
$ python measureandsave.py --data_root /data02/mcarvalh/softs/pix2pix/ --name nyu_multiscale --epoch latest
```

Flags:

--original_resolution: compares images using resolution of images in --path_gt. If necessary, images from --path_pred are resampled using --upsample_technique

<!-- --resample_technique: [nearest, bilinear, cubic] -->

--bad_minmax: depth values will be between [0.7133, 9.9955]

## Color images