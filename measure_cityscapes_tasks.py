from options.cityscape_options import Options
from modules.errormeasurement_cityscapes import ErrorMeasure
from ipdb import set_trace as st

opt = Options().parse()
errorob = ErrorMeasure(opt)
errorob.initialize()
errorob.calculate_all()

