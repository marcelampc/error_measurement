if [ "$#" -eq 2 ]
 then
  branch=$2
else
  branch=master
fi

echo $branch
git pull origin $branch
git add -A
git commit -m "$1"
git push origin $branch
