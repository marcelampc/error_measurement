import argparse
import os

# Measure and Save options

class GenOptions():
    def __init__(self):
        self.parser = argparse.ArgumentParser()
        # self.initialized =

    def initialize(self):
        self.parser = argparse.ArgumentParser()
        self.parser.add_argument('--data_root', help='root')
        self.parser.add_argument('--path_gt', help='path to ground truth \
                            files')
        self.parser.add_argument('--path_pred', help='path to label files')
        self.parser.add_argument('--results_path', default='./results')
        self.parser.add_argument('--epoch', default='latest')
        self.parser.add_argument('--up', default=0.8, help='up threshold to accuracy')
        self.parser.add_argument('--down', default=0.05, help='up threshold to accuracy')
        self.parser.add_argument('--name', help='if using data_root: name of the experiment, else: root name of the saved images')
        self.parser.add_argument('--plot', action='store_true')
        self.parser.add_argument('--phase', default='test')
        self.parser.add_argument('--bad_minmax', action='store_true', help='use bad min and max to calculate error')
        self.parser.add_argument('--original_resolution', action='store_true', help='adp')
        self.parser.add_argument('--resample_technique', default='bicubic', help='Options to resample: nearest, bilinear, cubic, bicubic (default)')
        self.parser.add_argument('--which_dataset', default='nyu')
        self.parser.add_argument('--extra_mask', action='store_true')
        self.parser.add_argument('--max_depth_mask', default=70.1, type=float, help='its usually gonna be 70 for make3d and 80, or 50 for KITTI')
        self.parser.add_argument('--path_extra_mask')
        # from gray to cmap
        self.parser.add_argument('--filename')
        self.parser.add_argument('--cmap', default='jet')
        self.parser.add_argument('--interpolation', default='spline16')
        self.parser.add_argument('--print_options', action='store_true')
        # ROB Challenge and KITTI dataset
        self.parser.add_argument('--min_depth', default=1e-3, help='value from Godard CVPR 2017')
        self.parser.add_argument('--max_depth', default=80, help='value from Godard CVPR 2017')
        self.parser.add_argument('--eigen_crop')
        self.parser.add_argument('--scale_to_mm', type=float, default=1000.0)
        self.parser.add_argument('--kitti_split', default='eigen_r')
        # opt = self.parser.parse_args()
        # print(opt)


    def parse(self):
        self.initialize()
        self.opt = self.parser.parse_args()

        if self.opt.print_options:
            # print information in a nice way:
            args = dict((arg, getattr(self.opt, arg)) for arg in dir(self.opt) if not arg.startswith('_'))
            print('---Options---')
            for k, v in sorted(args.items()):
                print('{}: {}'.format(k, v))

        return self.opt
