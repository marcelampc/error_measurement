import argparse
import os
from .general_options import GenOptions
# Measure and Save options

class RasterOptions(GenOptions):
    def initialize(self):
        GenOptions.initialize(self)
        self.parser.add_argument('--dfc_preprocessing', type=int, default=1)
        self.parser.add_argument('--offset', type=int, default=0)
        self.parser.add_argument('--mask_thres', type=float, default=9000)
        self.parser.add_argument('--which_raster', default='dsm', help='optios are: dsm, dsm_demb, dsm_dem3msr, dsm-demtli')
        self.parser.add_argument('--use_semantics', action='store_true')
        self.parser.add_argument('--normalize', action='store_true')
        self.parser.add_argument('--n_classes', type=int)
