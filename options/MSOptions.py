import argparse
import os
from .general_options import GenOptions
# Measure and Save options

class MSOptions(GenOptions):
    def initialize(self):
        GenOptions.initialize(self)