import argparse
import os
# Measure and Save options

class Options():
    def __init__(self):
        self.parser = argparse.ArgumentParser()

    def initialize(self):
        self.parser.add_argument('--tasks', nargs='+', default=['regression'], help='all tasks separated by a space')
        self.parser.add_argument('--outputs_nc', nargs='+', default=[1], type=int, help='all tasks separated by a space')
        self.parser.add_argument('--name')
        self.parser.add_argument('--scale', type=float, default=1000)
        self.parser.add_argument('--dataset_name')
        self.parser.add_argument('--data_split')

        self.parser.add_argument('--path_gt')
        self.parser.add_argument('--path_pred')

    def parse(self):
        self.initialize()
        self.opt = self.parser.parse_args()

        # if self.opt.print_options:
            # print information in a nice way:
        # args = dict((arg, getattr(self.opt, arg)) for arg in dir(self.opt) if not arg.startswith('_'))
        # print('---Options---')
        # for k, v in sorted(args.items()):
        #     print('{}: {}'.format(k, v))

        return self.opt