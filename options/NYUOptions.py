import argparse
import os
# Measure and Save options

class Options():
    def __init__(self):
        self.parser = argparse.ArgumentParser()
        # self.initialized =

    def initialize(self):
        self.parser = argparse.ArgumentParser()
        self.parser.add_argument('--path_gt', help='path to ground truth files')
        self.parser.add_argument('--path_pred', help='path to label files')
        self.parser.add_argument('--name')
        self.parser.add_argument('--scale', type=float, default=1000)
        self.parser.add_argument('--data_split', default='test')
        self.parser.add_argument('--dataset_name', default='nyu_795')
        self.parser.add_argument('--n_classes', default=14, type=int)

        self.parser.add_argument('--task', default='regression')
        
        # self.parser.add_argument('--results_path', default='./results')
        # self.parser.add_argument('--epoch', default='latest')
        # self.parser.add_argument('--up', default=0.8, help='up threshold to accuracy')
        # self.parser.add_argument('--down', default=0.05, help='up threshold to accuracy')
        # self.parser.add_argument('--name', help='if using data_root: name of the experiment, else: root name of the saved images')
        # self.parser.add_argument('--plot', action='store_true')
        # self.parser.add_argument('--bad_minmax', action='store_true', help='use bad min and max to calculate error')
        # self.parser.add_argument('--original_resolution', action='store_true', help='adp')
        # self.parser.add_argument('--resample_technique', default='bicubic', help='Options to resample: nearest, bilinear, cubic, bicubic (default)')
        # self.parser.add_argument('--which_dataset', default='nyu')
        # self.parser.add_argument('--extra_mask', action='store_true')
        # self.parser.add_argument('--max_depth_mask', default=70.1, type=float, help='its usually gonna be 70 for make3d and 80, or 50 for KITTI')
        # self.parser.add_argument('--path_extra_mask')
        # from gray to cmap
        # self.parser.add_argument('--filename')
        self.parser.add_argument('--print_options', action='store_true')

    def parse(self):
        self.initialize()
        self.opt = self.parser.parse_args()

        if self.opt.print_options:
            # print information in a nice way:
            args = dict((arg, getattr(self.opt, arg)) for arg in dir(self.opt) if not arg.startswith('_'))
            print('---Options---')
            for k, v in sorted(args.items()):
                print('{}: {}'.format(k, v))

        return self.opt